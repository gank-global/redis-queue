module bitbucket.org/gank-global/redis-queue

go 1.15

replace bitbucket.org/gank-global/newrelic-context v1.0.3 => github.com/best-expendables/newrelic-context v0.0.0-20200519100847-e4fe02cf0e14

require (
	bitbucket.org/gank-global/newrelic-context v1.0.3
	bitbucket.org/gank-global/rmq v0.0.0-20211201091402-927823c5a726
	github.com/best-expendables/logger v0.0.0-20200511084842-8247cf6c59bd
	github.com/best-expendables/newrelic-context v0.0.0-20200519103032-16e8dc9d4e57 // indirect
	github.com/best-expendables/trace v0.0.0-20200511055751-fb29d033fd2d
	github.com/best-expendables/user-service-client v0.0.0-20200511060456-3fcf8ea240f5
	github.com/go-redis/redis/v8 v8.11.4
	github.com/gofrs/uuid v3.3.0+incompatible
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/newrelic/go-agent v3.4.0+incompatible
	github.com/stretchr/testify v1.6.1 // indirect
	gopkg.in/redis.v5 v5.2.9 // indirect
	gorm.io/gorm v1.20.0
)
